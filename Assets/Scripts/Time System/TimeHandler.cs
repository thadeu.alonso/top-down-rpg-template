﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TimeHandler : MonoBehaviour
{
    [SerializeField] List<MonoBehaviour> _managesComponents;

    public void ChangeTime(bool isPaused)
    {
        foreach (var component in _managesComponents)
        {
            component.enabled = !isPaused;
        }
    }
}