﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    [SerializeField] BoolGlobalVariable _gamePaused;

    private TimeHandler[] _managedObjects;

    private void Awake()
    {
        _managedObjects = FindObjectsOfType<TimeHandler>();
    }

    private void OnEnable()
    {
        _gamePaused.OnValueChanged.AddListener((isGamePaused) => { UpdateTimeHandler(isGamePaused); });
    }

    private void OnDisable()
    {
        _gamePaused.OnValueChanged.RemoveListener((isGamePaused) => { UpdateTimeHandler(isGamePaused); });
    }

    public void PauseGame()
    {
        UpdateTimeHandler(true);
    }

    public void ResumeGame()
    {
        UpdateTimeHandler(false);
    }

    private void UpdateTimeHandler(bool isGamePaused)
    {
        Time.timeScale = isGamePaused ? 0f : 1f;

        foreach (var managed in _managedObjects)
        {
            managed.ChangeTime(isGamePaused);
        }
    }
}
