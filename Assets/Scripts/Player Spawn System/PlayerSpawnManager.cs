﻿using UnityEngine;

public class PlayerSpawnManager : MonoBehaviour
{
    [SerializeField] StringGlobalVariable _playerLastLocation;
    [SerializeField] TeleportPositionsConfig _sceneTeleports;
    [SerializeField] GameObject _playerPrefab;
    [SerializeField] string _respawnTag;

    public void SpawnPlayer()
    {
        if (!HasPlayerInScene())
        {
            var location = _sceneTeleports.Locations.Find(x => x.LastLocation == _playerLastLocation.Value);
            Vector2 position = location.Position;

            if (position == Vector2.zero)
                position = GameObject.FindGameObjectWithTag(_respawnTag).transform.position;

            var playerInstance = Instantiate(_playerPrefab, position, Quaternion.identity);

            var playerDirectionables = playerInstance.GetComponentsInChildren<IDirectionable>();

            foreach (var directionable in playerDirectionables)
            {
                directionable.SetDirection(Direction2D.GetVectorFrom(location.LookDirection));
            }
        }
    }

    private bool HasPlayerInScene()
    {
        return GameObject.FindGameObjectWithTag(_playerPrefab.tag) != null;
    }
}
