﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsableItemsSystem : MonoBehaviour
{
    [SerializeField] Sprite _hammerSprite;
    [SerializeField] SpriteUnityEvent OnChangeSprite;

    private void Start()
    {
        OnChangeSprite?.Invoke(_hammerSprite);
    }
}
