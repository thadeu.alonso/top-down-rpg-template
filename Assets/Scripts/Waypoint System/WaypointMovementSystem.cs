﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointMovementSystem : MonoBehaviour
{
    [SerializeField] List<Transform> _waypoints;

    [Header("Events")]
    [SerializeField] Vector2UnityEvent OnMoveToWaypoint;

    private Transform _currentWaypoint;
    private int _currentIndex;
    private bool _isStopped;

    private void Awake()
    {
        _currentWaypoint = _waypoints[0];
    }

    private void Update()
    {
        if (_isStopped)
            return;

        float distanceFromWaypoint = Vector2.Distance(transform.position, _currentWaypoint.position);

        if(distanceFromWaypoint > 0.1f)
        {
            Vector2 direction = (_currentWaypoint.position - transform.position).normalized;
            OnMoveToWaypoint?.Invoke(direction);
        }
        else
        {
            if (_currentIndex < _waypoints.Count - 1)
                _currentIndex++;
            else
                _currentIndex = 0;
            
            _currentWaypoint = _waypoints[_currentIndex];
        }
    }

    public void Stop()
    {
        _isStopped = true;
    }

    public void Resume()
    {
        _isStopped = false;
    }
}
