﻿[System.Serializable]
public struct AxisInput
{
    public string AxisName;
    public AxisInputType Type;
    [UnityEngine.SerializeField] public FloatUnityEvent Action;
}