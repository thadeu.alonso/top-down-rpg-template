﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystem : MonoBehaviour
{
    [SerializeField] List<KeyInput> _keyInputs;
    [SerializeField] List<AxisInput> _axisInputs;

    private void Update()
    {
        DetectKeys();
        DetectAxis();
    }

    private void DetectKeys()
    {
        foreach (var keyInput in _keyInputs)
        {
            switch (keyInput.Type)
            {
                case KeyInputType.KEY_PRESS:
                    if (Input.GetKeyDown(keyInput.Key))
                        keyInput.Action?.Invoke();
                    break;
                case KeyInputType.KEY_HOLD:
                    if (Input.GetKey(keyInput.Key))
                        keyInput.Action?.Invoke();
                    break;
                case KeyInputType.KEY_RELEASE:
                    if (Input.GetKeyUp(keyInput.Key))
                        keyInput.Action?.Invoke();
                    break;
            }
        }
    }

    private void DetectAxis()
    {
        foreach (var axisInput in _axisInputs)
        {
            float axis = 0f;

            switch (axisInput.Type)
            {
                case AxisInputType.AXIS:
                    axis = Input.GetAxis(axisInput.AxisName);
                    break;
                case AxisInputType.AXIS_RAW:
                    axis = Input.GetAxisRaw(axisInput.AxisName);
                    break;
            }

            axisInput.Action?.Invoke(axis);
        }
    }
}
