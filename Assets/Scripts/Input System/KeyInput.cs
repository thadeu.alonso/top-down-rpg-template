﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct KeyInput
{
    public string Name;
    public KeyCode Key;
    public KeyInputType Type;
    public UnityEvent Action;
}
