﻿public enum KeyInputType
{
    KEY_PRESS,
    KEY_HOLD,
    KEY_RELEASE
}