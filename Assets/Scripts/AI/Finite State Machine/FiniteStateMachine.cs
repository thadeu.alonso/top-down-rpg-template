﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine : MonoBehaviour
{
    protected FSMBaseState _initialState;
    protected FSMBaseState currentState;

    public FSMBaseState CurrentState
    {
        get { return currentState; }
    }

    private void Start()
    {
        TransitionToState(_initialState);
    }

    protected virtual void Update()
    {
        currentState.UpdateState(this);
    }

    public virtual void TransitionToState(FSMBaseState state)
    {
        if(currentState != null)
            currentState.ExitState(this);

        currentState = state;
        currentState.EnterState(this);
    }
}