﻿using UnityEngine;

public class BossAttackState : EntityBaseState
{
    private BossAnimationSystem _bossAnimSystem;
    private Vector3 _originalPosition;
    private float _originalMoveSpeed;
    private Vector3 _playerPosition;
    private bool _isAttackingPlayer;
    private float _minDistanceToAttack = 0.25f;

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "ATTACKING";

        _bossAnimSystem = fsm.GetComponentInChildren<BossAnimationSystem>();
        _bossAnimSystem.Attack();
        
        _originalPosition = fsm.transform.position;
        _originalMoveSpeed = _moveSystem.MovementSpeed;

        _playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        _moveSystem.MovementSpeed *= 3;
        
        Vector2 directionToPlayer = (_playerPosition - fsm.transform.position).normalized;
        _moveSystem.SetDirection(directionToPlayer);
        _moveSystem.Resume();
        _isAttackingPlayer = true;
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if (_isAttackingPlayer)
        {
            if (IsCloseEnough(fsm.transform.position, _playerPosition))
            {
                Vector2 directionToOriginal = (_originalPosition - fsm.transform.position).normalized;
                _moveSystem.SetDirection(directionToOriginal);
                _combatSystem.Attack();
                _isAttackingPlayer = false;
            }
        }
        else
        {
            if(IsCloseEnough(fsm.transform.position, _originalPosition))
            {
                _moveSystem.MovementSpeed = _originalMoveSpeed;
                fsm.TransitionToState(new BossMovingState());
            }
        }
    }

    private bool IsCloseEnough(Vector2 origin, Vector2 target)
    {
        return Vector2.Distance(origin, target) <= _minDistanceToAttack;
    }
}