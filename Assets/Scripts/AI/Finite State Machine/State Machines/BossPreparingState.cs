﻿using UnityEngine;

public class BossPreparingState : EntityBaseState
{
    private float _coolDown = 2f;
    private BossAnimationSystem _bossAnimSystem;
    private float _timer;

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "PREPARING";

        _bossAnimSystem = fsm.GetComponentInChildren<BossAnimationSystem>();

        _timer = _coolDown;
        _bossAnimSystem.PrepareToAttack();
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if(_timer <= 0f)
        {
            fsm.TransitionToState(new BossAttackState());
        }
        else
        {
            _timer -= Time.deltaTime;
        }
    }
}
