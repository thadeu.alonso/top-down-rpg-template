﻿public class EnemyFSM : CharacterFSM
{
    private void Awake()
    {
        _initialState = new EnemyIdleState();
        _dieState = new EnemyDeadState();
        _getHitState = new EnemyGetHitState(_combatSystem.HitRecoverDelay);
    }
}