﻿using System.Collections;
using System.Collections.Generic;

public class BossFSM : CharacterFSM
{
    private void Awake()
    {
        _initialState = new BossMovingState();
        _getHitState = new BossGetHitState(_hitRecoverDelay);
        _dieState = new BossDeadState();
    }
}