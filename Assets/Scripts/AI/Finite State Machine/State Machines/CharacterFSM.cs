﻿using UnityEngine;

public class CharacterFSM : FiniteStateMachine
{
    [SerializeField] protected float _hitRecoverDelay = 1f;
    [SerializeField] protected HealthSystem _healthSystem;
    [SerializeField] protected CombatSystem _combatSystem;

    protected FSMBaseState _dieState;
    protected FSMBaseState _getHitState;

    private void Awake()
    {
        _healthSystem = GetComponent<HealthSystem>();
        _combatSystem = GetComponent<CombatSystem>();
    }

    public virtual void Attack() { }

    public void Die()
    {
        TransitionToState(_dieState);
    }

    public void TakeHit()
    {
        if (_healthSystem.CurrentHealth > 0)
            TransitionToState(_getHitState);
        else
            TransitionToState(_dieState);
    }

    public override void TransitionToState(FSMBaseState state)
    {
        if (CurrentState == state)
            return;

        base.TransitionToState(state);
    }
}