﻿using UnityEngine;

public class PlayerRangedAttackingState : EntityBaseState
{
    private Vector2 _playerInput;

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "ATTACKING_RANGED";

        _moveSystem.Stop();
        _combatSystem.RangedAttack();

        //TODO: Provisório
        Delayer.DoAfterTime(fsm, () =>
        {
            fsm.TransitionToState(new PlayerIdleState());
        }, _combatSystem.AttackCooldown);
    }

    public override void ExitState(FiniteStateMachine fsm)
    {
    }

    public override void HandleInput(Vector2GlobalVariable playerInput)
    {
        _playerInput = playerInput.Value;
        _moveSystem.SetDirection(_playerInput);
        _charAnimSystem.SetDirection(_playerInput);
        _combatSystem.SetDirection(_playerInput);
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        
    }
}