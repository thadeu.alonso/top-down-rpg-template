﻿using UnityEngine;

public class BossDeadState : EntityBaseState
{
    private BossAnimationSystem _bossAnimSystem;
    private WaypointMovementSystem _waypointSystem;

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "DEAD";

        _bossAnimSystem = fsm.GetComponentInChildren<BossAnimationSystem>();
        _waypointSystem = fsm.GetComponent<WaypointMovementSystem>();
        fsm.GetComponent<Collider2D>().enabled = false;

        _waypointSystem.enabled = false;
        _moveSystem.Stop();
        _bossAnimSystem.Die();
    }
}