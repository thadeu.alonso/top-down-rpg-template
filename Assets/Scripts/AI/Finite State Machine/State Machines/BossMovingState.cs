﻿using UnityEngine;

public class BossMovingState : EntityBaseState
{
    private WaypointMovementSystem _waypointSystem;
    private BossAnimationSystem _bossAnimSystem;
    private float _coolDown = 5f;
    private float _timer;

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "MOVING";

        _waypointSystem = fsm.GetComponent<WaypointMovementSystem>();
        _bossAnimSystem = fsm.GetComponentInChildren<BossAnimationSystem>();

        _waypointSystem.Resume();
        _moveSystem.Resume();
        _timer = _coolDown;
        _bossAnimSystem.Move();
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if(_timer <= 0f)
        {
            fsm.TransitionToState(new BossPreparingState());
        }
        else
        {
            _timer -= Time.deltaTime;
        }
    }

    public override void ExitState(FiniteStateMachine fsm)
    {
        base.ExitState(fsm);
        _waypointSystem.Stop();
        _moveSystem.Stop();
    }
}
