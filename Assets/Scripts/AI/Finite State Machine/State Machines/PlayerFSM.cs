﻿using UnityEngine;

public class PlayerFSM : CharacterFSM
{
    [SerializeField] InteractionSystem _interactionSystem;
    [SerializeField] Vector2GlobalVariable _playerInput;

    private void Awake()
    {
        _initialState = new PlayerIdleState();
        _dieState = new PlayerDeadState();
        _getHitState = new PlayerGetHitState(_combatSystem.HitRecoverDelay);
    }

    protected override void Update()
    {
        base.Update();
        CurrentState.HandleInput(_playerInput);
    }

    public void Interact()
    {
        TransitionToState(new PlayerInteractingState());
    }

    // Called through Input System inspector
    public override void Attack()
    {
        if(!_interactionSystem.IsInteracting)
            TransitionToState(new PlayerAttackingState());
    }

    public void RangedAttack()
    {
        if(!_interactionSystem.IsInteracting)
            TransitionToState(new PlayerRangedAttackingState());
    }
}
