﻿using UnityEngine;

public class BossGetHitState : EntityBaseState
{
    private float _hitDelay;
    private float _hitTimer;

    public BossGetHitState(float hitDelay)
    {
        _hitDelay = hitDelay;
    }

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "HIT";

        _hitTimer = _hitDelay;
        fsm.GetComponent<WaypointMovementSystem>().Stop();
        _combatSystem.CancelCurrentAttack();
        _moveSystem.Stop();
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if (_hitTimer > 0f)
        {
            _hitTimer -= Time.deltaTime;
        }
        else
        {
            fsm.TransitionToState(new BossMovingState());
        }
    }
}