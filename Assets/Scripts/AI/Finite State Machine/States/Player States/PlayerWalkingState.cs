﻿using UnityEngine;

public class PlayerWalkingState : PlayerBaseState
{
    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "WALKING";

        _moveSystem.Resume();
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if (_playerInput == Vector2.zero)
            fsm.TransitionToState(new PlayerIdleState());
    }
}