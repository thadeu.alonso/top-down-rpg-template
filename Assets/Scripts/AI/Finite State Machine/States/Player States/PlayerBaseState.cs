﻿using UnityEngine;

public class PlayerBaseState : EntityBaseState
{
    protected InputSystem _inputSystem;
    protected InteractionSystem _interactionSystem;
    protected Vector2 _playerInput;

    public override void EnterState(FiniteStateMachine fsm)
    {
        _inputSystem = fsm.GetComponent<InputSystem>();
        _interactionSystem = fsm.GetComponent<InteractionSystem>();
        base.EnterState(fsm);
    }

    public override void HandleInput(Vector2GlobalVariable playerInput)
    {
        _playerInput = playerInput.Value;
        _interactionSystem.SetDirection(_playerInput);
        _combatSystem.SetDirection(_playerInput);
        _moveSystem.SetDirection(_playerInput);
        _charAnimSystem.SetDirection(_playerInput);
    }
}