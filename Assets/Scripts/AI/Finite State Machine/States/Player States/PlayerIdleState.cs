﻿using UnityEngine;

public class PlayerIdleState : PlayerBaseState
{
    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "IDLE";

        _inputSystem = fsm.GetComponent<InputSystem>();
        
        _inputSystem.enabled = true;
        _moveSystem.Stop();
        _charAnimSystem.StopWalking();
    }

    public override void ExitState(FiniteStateMachine fsm)
    {
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if (_playerInput != null && _playerInput != Vector2.zero)
            fsm.TransitionToState(new PlayerWalkingState());
    }
}