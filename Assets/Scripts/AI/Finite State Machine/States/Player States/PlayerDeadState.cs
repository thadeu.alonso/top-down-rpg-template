﻿using UnityEngine;
public class PlayerDeadState : PlayerBaseState
{
    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "DEAD";

        fsm.GetComponent<InputSystem>().enabled = false;
        fsm.GetComponent<Collider2D>().enabled = false;
        _charAnimSystem.Die();
        _moveSystem.Stop();
    }

    public override void ExitState(FiniteStateMachine fsm)
    {
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
    }
}