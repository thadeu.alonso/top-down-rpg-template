﻿using UnityEngine;

public class PlayerGetHitState : PlayerBaseState
{
    private float _delay;
    private float _hitTimer;

    public PlayerGetHitState(float delay)
    {
        _delay = delay;
    }

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "HIT";
        _hitTimer = _delay;
        _moveSystem.Stop();
        _interactionSystem.CancelInteraction();
    }

    public override void ExitState(FiniteStateMachine fsm)
    {
    }

    public override void HandleInput(Vector2GlobalVariable playerInput)
    {
        _playerInput = playerInput.Value;
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if(_hitTimer > 0f)
        {
            _hitTimer -= Time.deltaTime;
        }
        else
        {
            if (_healthSystem.CurrentHealth > 0)
            {
                if (_playerInput != Vector2.zero)
                {
                    fsm.TransitionToState(new PlayerWalkingState());
                }
                else
                {
                    fsm.TransitionToState(new PlayerIdleState());
                }
            }
            else
            {
                fsm.TransitionToState(new PlayerDeadState());
            }
        }
    }
}
