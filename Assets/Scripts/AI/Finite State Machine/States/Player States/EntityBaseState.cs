﻿public class EntityBaseState : FSMBaseState
{
    protected TopDownFreeMovement _moveSystem;
    protected CombatSystem _combatSystem;
    protected CharacterAnimationSystem _charAnimSystem;
    protected HealthSystem _healthSystem;

    public override void EnterState(FiniteStateMachine fsm)
    {
        _moveSystem     = fsm.GetComponent<TopDownFreeMovement>();
        _combatSystem   = fsm.GetComponent<CombatSystem>();
        _charAnimSystem = fsm.GetComponentInChildren<CharacterAnimationSystem>();
        _healthSystem   = fsm.GetComponent<HealthSystem>();
    }

    public override void ExitState(FiniteStateMachine fsm) { }
    public override void HandleInput(Vector2GlobalVariable playerInput) { }
    public override void UpdateState(FiniteStateMachine fsm) { }
}