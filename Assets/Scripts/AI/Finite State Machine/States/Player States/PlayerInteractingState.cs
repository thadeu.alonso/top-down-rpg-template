﻿using UnityEngine;

public class PlayerInteractingState : PlayerBaseState
{
    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "INTERACTING";

        _moveSystem.enabled = false;
        _combatSystem.enabled = false;
    }

    public override void ExitState(FiniteStateMachine fsm)
    {
        _moveSystem.enabled = true;
        _combatSystem.enabled = true;
    }

    public override void HandleInput(Vector2GlobalVariable playerInput)
    {
        _charAnimSystem.SetDirectionX(0f);
        _charAnimSystem.SetDirectionY(0f);
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if(!_interactionSystem.IsInteracting)
        {
            if(_playerInput != Vector2.zero)
                fsm.TransitionToState(new PlayerWalkingState());
            else
                fsm.TransitionToState(new PlayerIdleState());
        }
    }
}
