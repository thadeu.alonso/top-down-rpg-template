﻿using UnityEngine;

public class EnemyIdleState : EnemyBaseState
{
    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "IDLE";

        _moveSystem.Stop();
        _charAnimSystem.StopWalking();
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if (_targetSystem.IsTargetOnSight())
            fsm.TransitionToState(new EnemyChasingState());
    }
}
