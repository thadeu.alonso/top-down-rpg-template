﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeadState : FSMBaseState
{
    public override void EnterState(FiniteStateMachine fsm)
    {
        StateName = "DEAD";

        fsm.GetComponent<Collider2D>().enabled = false;
        fsm.GetComponent<TopDownFreeMovement>().Stop();
        fsm.GetComponentInChildren<CharacterAnimationSystem>().Die();
        fsm.GetComponent<TargetDetectionSystem>().enabled = false;
    }

    public override void ExitState(FiniteStateMachine fsm)
    {
    }

    public override void HandleInput(Vector2GlobalVariable playerInput)
    {
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
    }
}
