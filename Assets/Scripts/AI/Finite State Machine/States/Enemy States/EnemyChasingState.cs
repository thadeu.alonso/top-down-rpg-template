﻿using UnityEngine;

public class EnemyChasingState : EnemyBaseState
{
    private SpriteRenderer _enemySprite;

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "CHASING";

        _enemySprite = fsm.GetComponentInChildren<SpriteRenderer>();

        _moveSystem.Resume();
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if (!_targetSystem.IsTargetOnSight())
            fsm.TransitionToState(new EnemyIdleState());

        if (_targetSystem.IsTargetInAttackRange())
        {
            fsm.TransitionToState(new EnemyAttackState());
        }
        else
        {
            Vector2 targetDirection = _targetSystem.TargetDirection;

            _moveSystem.SetDirection(targetDirection);
            _combatSystem.SetDirection(targetDirection);
            _charAnimSystem.SetDirection(targetDirection);
            _enemySprite.flipX = targetDirection.x < 0;
        }
    }
}
