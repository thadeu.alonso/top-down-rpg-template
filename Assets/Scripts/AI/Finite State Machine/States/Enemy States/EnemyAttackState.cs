﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackState : EnemyBaseState
{
    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        StateName = "ATTACKING";

        _moveSystem.Stop();
        _charAnimSystem.StopWalking();
        _combatSystem.ResetAttackTimer();
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if(!_targetSystem.IsTargetInAttackRange() && _targetSystem.IsTargetOnSight())
            fsm.TransitionToState(new EnemyChasingState());
        
        if (_combatSystem.CanAttack)
            _combatSystem.Attack();
    }
}
