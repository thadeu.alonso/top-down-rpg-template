﻿public class EnemyBaseState : EntityBaseState
{
    protected TargetDetectionSystem _targetSystem;

    public override void EnterState(FiniteStateMachine fsm)
    {
        base.EnterState(fsm);
        _targetSystem = fsm.GetComponent<TargetDetectionSystem>();
    }
}
