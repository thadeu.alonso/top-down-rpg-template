﻿using UnityEngine;

public class EnemyGetHitState : FSMBaseState
{
    private float _delay;
    private float _hitTimer;
    private Vector2 _playerInput;

    public EnemyGetHitState(float delay)
    {
        _delay = delay;
    }

    public override void EnterState(FiniteStateMachine fsm)
    {
        StateName = "HIT";
        _hitTimer = _delay;
        fsm.GetComponent<CombatSystem>().CancelCurrentAttack();
    }

    public override void ExitState(FiniteStateMachine fsm)
    {
    }

    public override void HandleInput(Vector2GlobalVariable playerInput)
    {
        _playerInput = playerInput.Value;
    }

    public override void UpdateState(FiniteStateMachine fsm)
    {
        if (_hitTimer > 0f)
        {
            _hitTimer -= Time.deltaTime;
        }
        else
        {
            if (_playerInput != Vector2.zero)
                fsm.TransitionToState(new EnemyChasingState());
            else
                fsm.TransitionToState(new EnemyIdleState());
        }
    }
}