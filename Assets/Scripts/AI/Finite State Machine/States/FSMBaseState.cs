﻿using UnityEngine;

public abstract class FSMBaseState
{
    public string StateName { get; set; }

    public abstract void EnterState(FiniteStateMachine fsm);

    public abstract void ExitState(FiniteStateMachine fsm);

    public abstract void HandleInput(Vector2GlobalVariable playerInput);

    public abstract void UpdateState(FiniteStateMachine fsm);
}