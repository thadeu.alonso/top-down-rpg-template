﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public struct TargetEvents
{
    public Vector2UnityEvent OnUpdate;
    public Vector2UnityEvent OnTargetDetected;
    public Vector2UnityEvent OnTargetReached;
    public Vector2UnityEvent OnTargetLost;
}

public class TargetDetectionSystem : MonoBehaviour
{
    [SerializeField] bool _debugGizmos;
    [SerializeField] LayerMask _targetLayer;
    [SerializeField] float _sightRange = 1f;
    [SerializeField] float _stoppingDistance = 1f;
    [SerializeField] TargetEvents _events;

    public Vector2 TargetDirection { get; private set; }
    public float DistanceFromTarget { get; private set; }

    private void Update()
    {
        var _target = Physics2D.OverlapCircle(transform.position, _sightRange, _targetLayer);

        if(_target != null)
        {
            DistanceFromTarget = Vector2.Distance(transform.position, _target.transform.position);

            if (DistanceFromTarget < _sightRange && DistanceFromTarget >= _stoppingDistance)
            {
                TargetDirection = (_target.transform.position - transform.position).normalized;
                _events.OnTargetDetected?.Invoke(TargetDirection);
            }
            else if (DistanceFromTarget <= _stoppingDistance)
            {
                _events.OnTargetReached?.Invoke(TargetDirection);
            }
            else
            {
                TargetDirection = Vector2.zero;
                _events.OnTargetLost?.Invoke(TargetDirection);
            }

            _events.OnUpdate?.Invoke(TargetDirection);
        }
    }

    private void OnValidate()
    {
        if(_stoppingDistance > _sightRange)
        {
            _stoppingDistance = _sightRange;
            Debug.LogWarning("[TargetDetectionSystem] Stopping distance can't be higher than Sight Range");
        }
    }

    public bool IsTargetInAttackRange()
    {
        return DistanceFromTarget <= _stoppingDistance;
    }

    public bool IsTargetOnSight()
    {
        return DistanceFromTarget <= _sightRange;
    }

    private void OnDrawGizmos()
    {
        if (_debugGizmos)
        {
            Gizmos.color = IsTargetInAttackRange() ? Color.green : Color.red;
            Gizmos.DrawRay(transform.position, TargetDirection);

            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, _stoppingDistance);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, _sightRange);
        }
    }
}
