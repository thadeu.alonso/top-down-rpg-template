﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class TopDownFreeMovement : MonoBehaviour, IDirectionable
{
    [SerializeField] float _movementSpeed;

    private Rigidbody2D _rigidbody2D;
    private bool _isStopped;
    private Vector2 _direction;

    public float MovementSpeed { get => _movementSpeed; set => _movementSpeed = value; }

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public void SetDirection(Vector2 direction)
    {
        _direction = direction;
    }

    public void SetDirectionX(float x)
    {
        _direction.x = x;
    }

    public void SetDirectionY(float y)
    {
        _direction.y = y;
    }

    public void Resume()
    {
        _isStopped = false;
    }

    public void Stop()
    {
        _isStopped = true;
        _direction = Vector2.zero;
        _rigidbody2D.velocity = _direction;
    }

    public void FixedUpdate()
    {
        if (!_isStopped)
            _rigidbody2D.velocity = _direction.normalized * MovementSpeed * Time.fixedDeltaTime;
    }
}