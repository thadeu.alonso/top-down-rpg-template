﻿using UnityEngine.Events;
using System;

[Serializable]
public class ItemUnityEvent : UnityEvent<Item> { }