﻿using UnityEngine.Events;
using System;

[Serializable]
public class IntUnityEvent : UnityEvent<int> { }