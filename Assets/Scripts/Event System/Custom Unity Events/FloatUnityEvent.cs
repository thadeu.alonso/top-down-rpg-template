﻿using UnityEngine.Events;
using System;

[Serializable]
public class FloatUnityEvent : UnityEvent<float> { } 