﻿using UnityEngine.Events;
using System;
using UnityEngine;

[Serializable]
public class Vector3UnityEvent : UnityEvent<Vector3> { }