﻿using UnityEngine.Events;
using System;

[Serializable]
public class StringUnityEvent : UnityEvent<string> { }