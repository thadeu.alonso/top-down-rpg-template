﻿using UnityEngine.Events;
using System;

[Serializable]
public class BoolUnityEvent : UnityEvent<bool> { }