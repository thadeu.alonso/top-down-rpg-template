﻿using UnityEngine.Events;
using System;
using UnityEngine;

[Serializable]
public class Vector2UnityEvent : UnityEvent<Vector2> { }