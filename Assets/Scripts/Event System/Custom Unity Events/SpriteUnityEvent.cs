﻿using UnityEngine.Events;
using System;
using UnityEngine;

[Serializable]
public class SpriteUnityEvent : UnityEvent<Sprite> { }