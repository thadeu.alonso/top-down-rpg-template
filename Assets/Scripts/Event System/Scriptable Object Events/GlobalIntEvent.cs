﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Global Int Event", menuName = "Events/Global Int Event")]
public class GlobalIntEvent : ScriptableObject
{
	[SerializeField] bool _debugLog;

	private List<GlobalIntEventListener> listeners = new List<GlobalIntEventListener>();

	public void Raise(int value)
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
			listeners[i].OnEventRaised(value);
	}

	public void RegisterListener(GlobalIntEventListener listener)
	{
		listeners.Add(listener);

		if (_debugLog)
			Debug.Log($"[GLOBAL EVENT] {name} event has a new listener: {listener.name}");
	}

	public void UnregisterListener(GlobalIntEventListener listener)
	{
		listeners.Remove(listener);
	}
}
