﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSpriteEventListener : MonoBehaviour
{
	[SerializeField] GlobalSpriteEvent Event;
	[SerializeField] SpriteUnityEvent Response;

	private void OnEnable()
	{
		Event.RegisterListener(this);
	}

	private void OnDisable()
	{
		Event.UnregisterListener(this);
	}

	public void OnEventRaised(Sprite value)
	{
		Response?.Invoke(value);
	}
}