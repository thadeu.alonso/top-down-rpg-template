﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct GlobalEventResponse
{
	public string name;
	public GlobalEvent Event;
	public UnityEvent Response;
}

public class GlobalEventListener : MonoBehaviour, IEventListener
{
	[SerializeField] List<GlobalEventResponse> EventResponses;

	private void OnEnable()
	{
        foreach (var eventResponse in EventResponses)
        {
			eventResponse.Event.RegisterListener(this);
        }
	}

	private void OnDisable()
	{
        foreach (var eventResponse in EventResponses)
        {
			eventResponse.Event.UnregisterListener(this);
        }
	}

	public void OnEventRaised(GlobalEvent globalEvent)
	{
		EventResponses.Find(e => e.Event == globalEvent).Response?.Invoke();
	}
}