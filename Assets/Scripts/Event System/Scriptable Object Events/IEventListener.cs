﻿public interface IEventListener
{
	void OnEventRaised(GlobalEvent globalEvent);
}
