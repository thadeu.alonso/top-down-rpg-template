﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Global Event", menuName = "Events/Global Event")]
public class GlobalEvent : ScriptableObject
{
	[SerializeField] bool _debugLog;

	private List<IEventListener> listeners = new List<IEventListener>();

	public void Raise()
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
		{
			listeners[i].OnEventRaised(this);

			if (_debugLog)
				Debug.Log($"[GLOBAL EVENT] {listeners[i]} responds to {name} event");
		}
	}

	public void RegisterListener(IEventListener listener)
	{
		listeners.Add(listener);

		if (_debugLog)
			Debug.Log($"[GLOBAL EVENT] {name} event has a new listener: {listener}");
	}

	public void UnregisterListener(IEventListener listener)
	{ 
		listeners.Remove(listener); 
	}
}