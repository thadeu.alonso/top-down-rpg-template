﻿using UnityEngine;

public class GlobalBoolEventListener : MonoBehaviour
{
	[SerializeField] GlobalBoolEvent Event;
	[SerializeField] BoolUnityEvent Response;

	private void OnEnable()
	{
		Event.RegisterListener(this);
	}

	private void OnDisable()
	{
		Event.UnregisterListener(this);
	}

	public void OnEventRaised(bool value)
	{
		Response?.Invoke(value);
	}
}