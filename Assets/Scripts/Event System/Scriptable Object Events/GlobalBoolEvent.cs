﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Global Bool Event", menuName = "Events/Global Bool Event")]
public class GlobalBoolEvent : ScriptableObject
{
    [SerializeField] bool _debugLog;

	private List<GlobalBoolEventListener> listeners = new List<GlobalBoolEventListener>();

    public void Raise(bool value)
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
			listeners[i].OnEventRaised(value);
	}

	public void RegisterListener(GlobalBoolEventListener listener)
	{
		listeners.Add(listener);

		if (_debugLog)
			Debug.Log($"[GLOBAL EVENT] {name} event has a new listener: {listener.name}");
	}

	public void UnregisterListener(GlobalBoolEventListener listener)
	{
		listeners.Remove(listener);
	}
}