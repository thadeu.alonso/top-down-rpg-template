﻿using UnityEngine;

public class GlobalVector3EventListener : MonoBehaviour
{
	[SerializeField] GlobalVector3Event Event;
	[SerializeField] Vector3UnityEvent Response;

	private void OnEnable()
	{
		Event.RegisterListener(this);
	}

	private void OnDisable()
	{
		Event.UnregisterListener(this);
	}

	public void OnEventRaised(Vector3 value)
	{
		Response?.Invoke(value);
	}
}
