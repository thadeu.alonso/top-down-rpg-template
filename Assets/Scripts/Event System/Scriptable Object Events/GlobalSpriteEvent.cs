﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Global Sprite Event", menuName = "Events/Global Sprite Event")]
public class GlobalSpriteEvent : ScriptableObject
{
	[SerializeField] bool _debugLog;

	private List<GlobalSpriteEventListener> listeners = new List<GlobalSpriteEventListener>();

	public void Raise(Sprite value)
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
			listeners[i].OnEventRaised(value);
	}

	public void RegisterListener(GlobalSpriteEventListener listener)
	{
		listeners.Add(listener);

		if (_debugLog)
			Debug.Log($"[GLOBAL EVENT] {name} event has a new listener: {listener.name}");
	}

	public void UnregisterListener(GlobalSpriteEventListener listener)
	{
		listeners.Remove(listener);
	}
}