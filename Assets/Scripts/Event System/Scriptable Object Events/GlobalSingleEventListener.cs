﻿using UnityEngine;
using UnityEngine.Events;

public class GlobalSingleEventListener : MonoBehaviour, IEventListener
{
	[SerializeField] GlobalEvent Event;
	[SerializeField] UnityEvent Response;

	private void OnEnable()
	{ 
		Event.RegisterListener(this); 
	}

	private void OnDisable()
	{ 
		Event.UnregisterListener(this); 
	}

	public void OnEventRaised(GlobalEvent globalEvent)
	{ 
		Response.Invoke(); 
	}
}