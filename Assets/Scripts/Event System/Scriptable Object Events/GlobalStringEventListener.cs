﻿using UnityEngine;
public class GlobalStringEventListener : MonoBehaviour
{
	[SerializeField] GlobalStringEvent Event;
	[SerializeField] StringUnityEvent Response;

	private void OnEnable()
	{
		Event.RegisterListener(this);
	}

	private void OnDisable()
	{
		Event.UnregisterListener(this);
	}

	public void OnEventRaised(string value)
	{
		Response?.Invoke(value);
	}
}