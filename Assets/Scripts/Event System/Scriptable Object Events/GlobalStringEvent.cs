﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Global String Event", menuName = "Events/Global String Event")]
public class GlobalStringEvent : ScriptableObject
{
    [SerializeField] bool _debugLog;

	private List<GlobalStringEventListener> listeners = new List<GlobalStringEventListener>();

    public void Raise(string value)
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
			listeners[i].OnEventRaised(value);
	}

	public void RegisterListener(GlobalStringEventListener listener)
	{
		listeners.Add(listener);

		if (_debugLog)
			Debug.Log($"[GLOBAL EVENT] {name} event has a new listener: {listener.name}");
	}

	public void UnregisterListener(GlobalStringEventListener listener)
	{
		listeners.Remove(listener);
	}
}