﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ItemEventResponse
{
	public string name;
	public GlobalItemEvent Event;
	public ItemUnityEvent Response;
}

public class GlobalItemEventListener : MonoBehaviour
{
	[SerializeField] List<ItemEventResponse> Responses;

	private void OnEnable()
	{
        foreach (var response in Responses)
        {
			response.Event.RegisterListener(this);
        }
	}

	private void OnDisable()
	{
        foreach (var response in Responses)
        {
			response.Event.UnregisterListener(this);
        }
	}

	public void OnEventRaised(GlobalItemEvent itemEvent, Item value)
	{
		Responses.Find(r => r.Event == itemEvent).Response?.Invoke(value);
	}
}