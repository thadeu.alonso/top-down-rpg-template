﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Global Vector3 Event", menuName = "Events/Global Vector3 Event")]
public class GlobalVector3Event : ScriptableObject
{
    [SerializeField] bool _debugLog;

	private List<GlobalVector3EventListener> listeners = new List<GlobalVector3EventListener>();

    public void Raise(Vector3 value)
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
		{
			listeners[i].OnEventRaised(value);

			if (_debugLog)
				Debug.Log($"[GLOBAL EVENT] {listeners[i]} responds to {name} event");
		}
	}

	public void RegisterListener(GlobalVector3EventListener listener)
	{
		listeners.Add(listener);

		if (_debugLog)
			Debug.Log($"[GLOBAL EVENT] {name} event has a new listener: {listener.name}");
	}

	public void UnregisterListener(GlobalVector3EventListener listener)
	{
		listeners.Remove(listener);
	}
}