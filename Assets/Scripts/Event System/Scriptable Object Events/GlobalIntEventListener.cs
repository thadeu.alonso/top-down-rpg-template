﻿using UnityEngine;

public class GlobalIntEventListener : MonoBehaviour
{
	[SerializeField] GlobalIntEvent Event;
	[SerializeField] IntUnityEvent Response;

	private void OnEnable()
	{
		Event.RegisterListener(this);
	}

	private void OnDisable()
	{
		Event.UnregisterListener(this);
	}

	public void OnEventRaised(int value)
	{
		Response?.Invoke(value);
	}
}