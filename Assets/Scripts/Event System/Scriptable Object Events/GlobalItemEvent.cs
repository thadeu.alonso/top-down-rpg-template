﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Global Item Event", menuName = "Events/Global Item Event")]
public class GlobalItemEvent : ScriptableObject
{
	[SerializeField] bool _debugLog;

	private List<GlobalItemEventListener> listeners = new List<GlobalItemEventListener>();

	public void Raise(Item value)
	{
		for (int i = listeners.Count - 1; i >= 0; i--)
		{
			listeners[i].OnEventRaised(this, value);

			if (_debugLog)
				Debug.Log($"[GLOBAL EVENT] {listeners[i]} responds to {name} event");
		}
	}

	public void RegisterListener(GlobalItemEventListener listener)
	{
		listeners.Add(listener);

		if (_debugLog)
			Debug.Log($"[GLOBAL EVENT] {name} event has a new listener: {listener}");
	}

	public void UnregisterListener(GlobalItemEventListener listener)
	{
		listeners.Remove(listener);
	}
}