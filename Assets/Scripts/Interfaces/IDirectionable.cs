﻿using UnityEngine;

public interface IDirectionable
{
    void SetDirectionX(float x);
    void SetDirectionY(float y);
    void SetDirection(Vector2 direction);
}
