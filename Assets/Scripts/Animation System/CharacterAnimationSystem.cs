﻿using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class CharacterAnimationSystem : MonoBehaviour, IDirectionable, IHittable
{
    [SerializeField] bool _flipSpriteOnMove;

    public bool IsWalking { get; set; }
   
    private Animator _animator;
    private SpriteRenderer _sprite;
    private Vector2 _direction;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _sprite = GetComponent<SpriteRenderer>();
    }

    public void Update()
    {
        if (_direction.x != 0f || _direction.y != 0f) {

            IsWalking = true;

            UpdateAnimatorDirections();

            if(_flipSpriteOnMove)
                _sprite.flipX = _direction.x < 0f;
        }
        else
        {
            IsWalking = false;
        }
        
        _animator.SetBool("IsWalking", IsWalking);
    }

    public void SetDirectionX(float x)
    {
        _direction.x = x;
    }

    public void SetDirectionY(float y)
    {
        _direction.y = y;
    }

    public void SetDirection(Vector2 direction)
    {
        if (direction.x != 0f || direction.y != 0f)
        {
            _direction = direction;
            UpdateAnimatorDirections();   
        }
    }

    private void UpdateAnimatorDirections()
    {
        _animator.SetFloat("Horizontal", _direction.x);
        _animator.SetFloat("Vertical", _direction.y);
    }

    public void StopWalking()
    {
        _direction = Vector2.zero;
        IsWalking = false;
    }

    public void Attack()
    {
        _animator.SetTrigger("Attack");
    }

    public void TakeHit()
    {
        _animator.SetTrigger("GetHit");
    }

    public void Die()
    {
        _animator.SetTrigger("Dies");
    }
}