﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class BossAnimationSystem : MonoBehaviour, IHittable
{
    private Animator _animator;
    private SpriteRenderer _sprite;
    private Vector2 _direction;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _sprite = GetComponent<SpriteRenderer>();
    }

    public void Die()
    {
        _animator.SetTrigger("Dies");
    }

    public void Move()
    {
        _animator.SetBool("IsPreparing", false);
    }

    public void PrepareToAttack()
    {
        _animator.SetBool("IsPreparing", true);
    }

    public void Attack()
    {
        _animator.SetBool("IsPreparing", false);
        _animator.SetTrigger("Attack");
    }

    public void TakeHit()
    {
        _animator.SetTrigger("GetHit");
    }
}
