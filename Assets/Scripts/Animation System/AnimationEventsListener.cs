﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEventsListener : MonoBehaviour
{
    [SerializeField] UnityEvent OnStart;
    [SerializeField] UnityEvent OnFinish;

    public void OnAnimationStarts()
    {
        OnStart?.Invoke();
    }

    public void OnAnimationFinishes()
    {
        OnFinish?.Invoke();
    }
}
