﻿using UnityEngine;

[CreateAssetMenu(fileName = "Vector2 Global Variable", menuName = "Global Variables/Vector2")]
public class Vector2GlobalVariable : ScriptableObject, ISerializationCallbackReceiver
{
    public Vector2 InitialValue;
    public Vector2 Value
    {
        get { return _value; }
        set { _value = value; OnValueChanged?.Invoke(Value); }
    }

    [Header("Events")]
    public Vector2UnityEvent OnValueChanged;

    private Vector2 _value;

    public void SetX(float value)
    {
        _value.x = value;
    }

    public void SetY(float value)
    {
        _value.y = value;
    }

    public void OnAfterDeserialize()
    {
        Value = InitialValue;
    }

    public void OnBeforeSerialize() { }
}