﻿using UnityEngine;

[CreateAssetMenu(fileName = "Bool Global Variable", menuName = "Global Variables/Bool")]
public class BoolGlobalVariable : ScriptableObject, ISerializationCallbackReceiver
{
    public bool InitialValue;
    public bool Value
    {
        get { return _value; }
        set { _value = value; OnValueChanged?.Invoke(Value); }
    }

    private bool _value;

    [Header("Events")]
    public BoolUnityEvent OnValueChanged;

    public void Toggle()
    {
        Value = !Value;
    }

    public void OnAfterDeserialize()
    {
        Value = InitialValue;
    }

    public void OnBeforeSerialize() { }
}
