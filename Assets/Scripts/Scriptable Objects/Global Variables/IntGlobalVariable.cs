﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Int Global Variable", menuName = "Global Variables/Int")]
public class IntGlobalVariable : ScriptableObject, ISerializationCallbackReceiver
{
    public int InitialValue;
    public int Value 
    { 
        get { return _value; } 
        set { _value = value; OnValueChanged?.Invoke(Value); } 
    }

    private int _value;

    [Header("Events")]
    public IntUnityEvent OnValueChanged;

    public void OnAfterDeserialize()
    {
        Value = InitialValue;
    }

    public void OnBeforeSerialize() { }
}
