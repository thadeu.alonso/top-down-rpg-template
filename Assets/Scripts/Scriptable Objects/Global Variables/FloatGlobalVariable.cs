﻿using UnityEngine;

[CreateAssetMenu(fileName = "Float Global Variable", menuName = "Global Variables/Float")]
public class FloatGlobalVariable : ScriptableObject, ISerializationCallbackReceiver
{
    public float InitialValue;
    public float Value
    {
        get { return _value; }
        set { _value = value; OnValueChanged?.Invoke(Value); }
    }

    [Header("Events")]
    public FloatUnityEvent OnValueChanged;

    private float _value;

    public void OnAfterDeserialize()
    {
        Value = InitialValue;
    }

    public void OnBeforeSerialize() { }
}
