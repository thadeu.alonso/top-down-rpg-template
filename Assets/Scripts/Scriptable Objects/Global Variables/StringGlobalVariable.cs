﻿using UnityEngine;

[CreateAssetMenu(fileName = "String Global Variable", menuName = "Global Variables/String")]
public class StringGlobalVariable : ScriptableObject, ISerializationCallbackReceiver
{
    public string InitialValue;
    public string Value
    {
        get { return _value; }
        set { _value = value; OnValueChanged?.Invoke(Value); }
    }

    private string _value;

    [Header("Events")]
    public StringUnityEvent OnValueChanged;

    public void OnAfterDeserialize()
    {
        Value = InitialValue;
    }

    public void OnBeforeSerialize() { }
}