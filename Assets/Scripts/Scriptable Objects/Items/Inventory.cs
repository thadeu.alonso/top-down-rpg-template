﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory")]
public class Inventory : ScriptableObject, ISerializationCallbackReceiver
{
    public List<Item> Items;
    public ItemUnityEvent OnItemAdded;
    public ItemUnityEvent OnItemRemoved;

    public void AddItem(Item item)
    {
        if (Items.Contains(item) && item.isUnique)
        {
            Debug.LogWarning($"[Inventory] Can't add item {item.Name}, already in Inventory!");
        }
        else
        {
            Items.Add(item);
            OnItemAdded?.Invoke(item);
        }
    }

    public void RemoveItem(Item item)
    {
        Items.Remove(item);
        OnItemRemoved?.Invoke(item);
    }

    public void OnAfterDeserialize()
    {
        Items = new List<Item>();
    }

    public void OnBeforeSerialize() { }

    public bool HasItem(Item item)
    {
        return Items.Contains(item);
    }
}