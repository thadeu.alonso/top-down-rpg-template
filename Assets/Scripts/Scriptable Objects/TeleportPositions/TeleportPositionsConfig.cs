﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTeleportPositions", menuName = "Configs/Teleport Positions")]
public class TeleportPositionsConfig : ScriptableObject
{
    public List<LocationPosition> Locations;
}

[System.Serializable]
public struct LocationPosition
{
    public string LastLocation;
    public Vector2 Position;
    public EDirection LookDirection;
}