﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DialogSequence : ScriptableObject
{
    public string SpeakerName;
    public List<Line> Sequence;
}

[System.Serializable]
public struct Line
{
    [TextArea]
    public string line;
}