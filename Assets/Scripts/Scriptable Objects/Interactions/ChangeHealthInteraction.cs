﻿using UnityEngine;

[CreateAssetMenu(fileName = "Change Health Interaction", menuName = "Interactions/Change Health")]
public class ChangeHealthInteraction : ScriptableObject
{
    public int _healthAmount;

    public void Execute(GameObject target)
    {
        HealthSystem healthSystem = target.GetComponent<HealthSystem>();

        if(healthSystem != null)
        {
            if (_healthAmount < 0)
                healthSystem.LoseHealth(_healthAmount * -1);
            else if (_healthAmount > 0)
                healthSystem.GainHealth(_healthAmount);
        }
    }
}