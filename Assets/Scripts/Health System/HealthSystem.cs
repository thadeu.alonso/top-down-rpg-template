﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Events
{
    [SerializeField] public IntUnityEvent OnGainHealth;
    [SerializeField] public IntUnityEvent OnLoseHealth;
    [SerializeField] public UnityEvent OnDie;
}
public class HealthSystem : MonoBehaviour
{
    [SerializeField] int _initialHealth;
    [SerializeField] int _maxHealth;
    [SerializeField] Events _events;

    public int CurrentHealth { get; private set; }

    private void Awake()
    {
        GainHealth(_maxHealth);
    }

    public void GainHealth(int amount)
    {
        if(CurrentHealth < _maxHealth)
        {
            CurrentHealth += amount;
        }

        if(CurrentHealth > _maxHealth)
        {
            CurrentHealth = _maxHealth;
        }

        _events.OnGainHealth?.Invoke(CurrentHealth);
    }

    public void LoseHealth(int amount)
    {
        if(CurrentHealth >= amount)
        {
            CurrentHealth -= amount;
        }
        
        if(CurrentHealth <= 0)
        {
            CurrentHealth = 0;
            _events.OnDie?.Invoke();
        }

        _events.OnLoseHealth?.Invoke(CurrentHealth);
    }
}
