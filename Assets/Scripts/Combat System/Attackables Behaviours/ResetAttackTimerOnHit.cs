﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetAttackTimerOnHit : MonoBehaviour, IAttackable
{
    [SerializeField] CombatSystem _combatSystem;

    public void OnAttack(GameObject attacker)
    {
        _combatSystem.ResetAttackTimer();
    }
}
