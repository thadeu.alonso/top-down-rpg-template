﻿using UnityEngine;

public class PlayHitAnimationOnHit : MonoBehaviour, IAttackable
{
    [SerializeField] MonoBehaviour _characterAnimation;

    private IHittable _hittable;

    private void Awake()
    {
        _hittable = _characterAnimation.GetComponent<IHittable>();
    }

    private void OnValidate()
    {
        if (_characterAnimation != null)
        {
            var hittable = _characterAnimation.GetComponent<IHittable>();

            if (hittable == null)
            {
                Debug.LogError("[PlayHitAnimationOnHit] Script doesn't implement IHittable interface!");
                _characterAnimation = null;
            }
        }
    }

    public void OnAttack(GameObject attacker)
    {
        _hittable.TakeHit();
    }
}