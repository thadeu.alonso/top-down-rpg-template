﻿using UnityEngine;

public class TakeDamageOnHit : MonoBehaviour, IAttackable
{
    [SerializeField] HealthSystem _healthSystem;

    public void OnAttack(GameObject attacker)
    {
        int damage = attacker.GetComponent<CombatSystem>().CurrentDamage;

        if (_healthSystem != null)
        {
            _healthSystem.LoseHealth(damage);
            Debug.Log($"{gameObject.name} Take {damage} damage from {attacker.name}");
        }
    }
}
