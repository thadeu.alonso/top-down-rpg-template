﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyKnobackOnHit : MonoBehaviour, IAttackable
{
    [SerializeField] float _knockbackForce = 300f;

    public void OnAttack(GameObject attacker)
    {
        Vector2 attackerOppositeDirection = (attacker.transform.position - transform.position).normalized;
        GetComponent<Rigidbody2D>().AddForce(-attackerOppositeDirection * _knockbackForce, ForceMode2D.Impulse);
    }
}
