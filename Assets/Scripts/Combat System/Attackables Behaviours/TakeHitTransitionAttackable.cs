﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeHitTransitionAttackable : MonoBehaviour, IAttackable
{
    [SerializeField] GameObjectUnityEvent OnTakeHit;

    public void OnAttack(GameObject attacker)
    {
        OnTakeHit?.Invoke(attacker);
    }
}
