using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowMagicBallOnHit : MonoBehaviour, IAttackable
{
    public void OnAttack(GameObject attacker)
    {
        attacker.GetComponent<CombatSystem>().ThrowMagicBall();
    }
}
