﻿using System;
using UnityEngine;

[RequireComponent(typeof(MoveObject))]
public class MagicBall : MonoBehaviour
{
    public event Action OnHitTarget;

    private MoveObject _moveObject;
    private Transform _player;
    private bool _isInitialized;

    public void Setup(Transform player)
    {
        if (!_isInitialized)
        {
            _player = player;
            _moveObject = GetComponent<MoveObject>();
            _isInitialized = true;
        }
    }

    public void SetDestination(Vector2 rangedAttackDestination)
    {
        gameObject.SetActive(true);
        transform.localPosition = Vector2.zero;
        transform.SetParent(null);
        _moveObject.Move(rangedAttackDestination);
    }

    public void ReturnToPlayer()
    {
        _moveObject.Move(_player.position);
        _moveObject.Events.OnDestinationReached.AddListener(OnReachedPlayer);
    }

    private void OnReachedPlayer()
    {
        Reset();
        _moveObject.Events.OnDestinationReached.RemoveListener(OnReachedPlayer);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        IAttackable[] attacksBehaviour = collision.transform.GetComponents<IAttackable>();

        if (attacksBehaviour != null)
        {
            foreach (IAttackable behaviour in attacksBehaviour)
            {
                behaviour.OnAttack(_player.gameObject);
            }
        }
    }

    public void Reset()
    {
        StopAllCoroutines();
        transform.SetParent(_player);
        gameObject.SetActive(false);
    }
}