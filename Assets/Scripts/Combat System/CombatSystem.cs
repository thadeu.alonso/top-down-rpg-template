﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CombatSystem : MonoBehaviour, IDirectionable
{
    [Header("General")]
    [SerializeField] LayerMask _attackableLayer;
    [SerializeField] TriggerDetector _hitbox;

    [Header("Ranged attack")]
    [SerializeField] GameObject _magicBallObject;
    [SerializeField] float _rangedDistance;

    [Header("Melee attack")]
    [SerializeField] int _damage;
    [SerializeField] Vector2 _size;
    [SerializeField] float _distance;
    [SerializeField] float _hitDelay;
    [SerializeField] float _hitRecoverDelay;
    [SerializeField] float _attackCooldown;

    [Header("Events")]
    [SerializeField] UnityEvent OnAttack;

    public int CurrentDamage => _damage;
    public bool CanAttack => _canAttack;
    public float AttackCooldown => _attackCooldown;
    public float HitRecoverDelay => _hitRecoverDelay;
    public float RangedDistance => _rangedDistance;
    public Vector2 FacingDirection => _facingDirection;

    private Vector2 _facingDirection = Vector2.right;
    private bool _canAttack = true;
    private float _attackTimer;
    private MagicBall _magicBall;
    private bool _hasAlreadyThrowBall;

    private void Start()
    {
        if (_magicBallObject != null)
        {
            _magicBall = _magicBallObject.GetComponent<MagicBall>();
            _magicBall.Setup(transform);
            _magicBall.OnHitTarget += ResetMagicBall;
        }
    }

    private void Update()
    {
        if (_attackTimer > 0f)
        {
            _attackTimer -= Time.deltaTime;
        }

        _canAttack = _attackTimer <= 0f;
    }

    public void Attack()
    {
        if (!_canAttack)
            return;

        float boxPosX = transform.position.x + (_facingDirection.x * _distance);
        float boxPosY = transform.position.y + (_facingDirection.y * _distance);
        Vector2 boxPoint = new Vector2(boxPosX, boxPosY);

        Collider2D[] collidersHit = Physics2D.OverlapBoxAll(boxPoint, _size, 0f, _attackableLayer);

        if (collidersHit != null)
        {
            foreach (var collider in collidersHit)
            {
                var attacksBehaviour = collider.transform.GetComponents<IAttackable>();

                if (attacksBehaviour != null)
                {
                    Delayer.DoAfterTime(this, () => { HitTarget(attacksBehaviour); }, _hitDelay);
                }
            }
        }

        ResetAttackTimer();
        OnAttack?.Invoke();
    }

    public void RangedAttack()
    {
        if (_hasAlreadyThrowBall)
        {
            _magicBall.ReturnToPlayer();
            _hasAlreadyThrowBall = false;
        }
        else
        {
            _hasAlreadyThrowBall = true;
            ThrowMagicBall();
        }
    }

    public void ThrowMagicBall()
    {
        float boxPosX = transform.position.x + (_facingDirection.x * _rangedDistance);
        float boxPosY = transform.position.y + (_facingDirection.y * _rangedDistance);
        Vector2 rangedAttackDestination = new Vector2(boxPosX, boxPosY);
        _magicBall.SetDestination(rangedAttackDestination);
    }

    public void ResetMagicBall()
    {
        _hasAlreadyThrowBall = false;
        _magicBall.Reset();
    }

    public void ResetAttackTimer()
    {
        _attackTimer = _attackCooldown;
    }

    public void CancelCurrentAttack()
    {
        StopAllCoroutines();
    }

    public void SetDirectionX(float x)
    {
        if (x != 0f)
            _facingDirection = new Vector2(x, 0f);
    }

    public void SetDirectionY(float y)
    {
        if (y != 0f)
            _facingDirection = new Vector2(0f, y);
    }

    public void SetDirection(Vector2 direction)
    {
        if(direction != Vector2.zero)
            _facingDirection = direction;
    }

    private void HitTarget(IAttackable[] attackables)
    {
        _canAttack = false;

        foreach (var attackBehaviour in attackables)
        {
            attackBehaviour.OnAttack(gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        float boxPosX = transform.position.x + (_facingDirection.x * _distance);
        float boxPosY = transform.position.y + (_facingDirection.y * _distance);
        Vector2 boxPoint = new Vector2(boxPosX, boxPosY);
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boxPoint, Vector2.one * _size);
    }
}