﻿using UnityEngine;

public static class Direction2D
{
    public static Vector3 GetVectorFrom(EDirection direction)
    {
        Vector3 vector = Vector3.zero;

        switch (direction)
        {
            case EDirection.UP: vector = Vector3.up; break;
            case EDirection.DOWN: vector = Vector3.down; break;
            case EDirection.RIGHT: vector = Vector3.right; break;
            case EDirection.LEFT: vector = Vector3.left; break;
        }

        return vector;
    }

    public static EDirection GetDirectionFrom(Vector3 vector)
    {
        EDirection direction = EDirection.NONE;

        if (vector == Vector3.down)         direction = EDirection.DOWN;
        else if (vector == Vector3.up)      direction = EDirection.UP;
        else if (vector == Vector3.left)    direction = EDirection.LEFT;
        else if (vector == Vector3.right)   direction = EDirection.RIGHT;

        return direction;
    }
}