﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCameraEdge : MonoBehaviour
{
    [SerializeField] string _targetTag;
    [SerializeField] Vector3UnityEvent OnReachEdge;

    private Transform _targetTransform;

    private void Awake()
    {
        _targetTransform = GameObject.FindGameObjectWithTag(_targetTag).transform;
    }

    private void Update()
    {
        EDirection direction = EDirection.NONE;
        Vector2 screenPos = Camera.main.WorldToScreenPoint(_targetTransform.position);

        if (screenPos.x < 0f)               direction = EDirection.LEFT;
        if (screenPos.x > Screen.width)     direction = EDirection.RIGHT;
        if (screenPos.y < 0f)               direction = EDirection.DOWN;
        if (screenPos.y > Screen.height)    direction = EDirection.UP;

        if(direction != EDirection.NONE)
        {
            Vector3 vector = Direction2D.GetVectorFrom(direction);
            OnReachEdge?.Invoke(vector);
        }
    }
}