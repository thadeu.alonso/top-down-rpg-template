﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogSystem : MonoBehaviour
{
    [SerializeField] GameObject _dialogBackground;
    [SerializeField] TextMeshProUGUI _dialogText;

    public void ShowDialog(string text)
    {
        _dialogBackground.SetActive(true);
        _dialogText.text = text;
    }

    public void CloseDialog()
    {
        _dialogBackground.SetActive(false);
    }
}
