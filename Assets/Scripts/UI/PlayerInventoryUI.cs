﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventoryUI : MonoBehaviour
{
    [SerializeField] GameObject _itemPrefab;
    [SerializeField] RectTransform _layoutTransform;
    [SerializeField] Inventory _playerInventory;

    private Dictionary<Item, GameObject> _instantiatedItems;

    private void Start()
    {
        _instantiatedItems = new Dictionary<Item, GameObject>();

        foreach (var item in _playerInventory.Items)
        {
            AddItem(item);
        }
    }

    public void AddItem(Item item)
    {
        var itemGameObject = Instantiate(_itemPrefab, _layoutTransform);
        var itemUI = itemGameObject.GetComponent<ItemUI>();

        _instantiatedItems.Add(item, itemGameObject);
        itemUI.Setup(item);
    }

    public void RemoveItem(Item item)
    {
        GameObject itemGO;

        if (_instantiatedItems.TryGetValue(item, out itemGO))
        {
            if(itemGO != null)
            {
                Destroy(itemGO);
                _instantiatedItems.Remove(item);
            }
        }
    }
}