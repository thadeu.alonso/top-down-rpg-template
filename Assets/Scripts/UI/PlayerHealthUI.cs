﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : MonoBehaviour
{
    [SerializeField] IntGlobalVariable _playerHealth;
    [SerializeField] GameObject _heartIconPrefab;
    [SerializeField] RectTransform _layoutTransform;

    private List<Image> _healthIcons;

    private void Start()
    {
        _playerHealth.OnValueChanged.AddListener(OnHealthChanged);
        _healthIcons = new List<Image>();

        for (int i = 0; i < _playerHealth.Value; i++)
        {
            var iconGameObject = Instantiate(_heartIconPrefab, _layoutTransform);
            Image iconImage = iconGameObject.GetComponent<Image>();
            _healthIcons.Add(iconImage);
        }
    }

    private void OnHealthChanged(int value)
    {
        for (int i = 0; i < _healthIcons.Count; i++)
        {
            if(i < value)
            {
                _healthIcons[i].color = Color.white;
            }
            else
            {
                _healthIcons[i].color = Color.grey;
            }
        }
    }
}
