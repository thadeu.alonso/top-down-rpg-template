﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public struct ScreenFadeEvents
{
    public UnityEvent OnFadeOutEnds;
    public UnityEvent OnFadeInEnds;
}

public class ScreenFade : MonoBehaviour
{
    [SerializeField] CanvasGroup _canvasGroup;
    [SerializeField] float _transitionTime;
    [SerializeField] bool _igoreTimeScale;
    [SerializeField] ScreenFadeEvents _events;

    private float _timer;

    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void FadeOut()
    {
        _canvasGroup.alpha = 0f;
        StartCoroutine(Transition(0f, 1f, _events.OnFadeOutEnds));
    }

    public void FadeIn()
    {
        _canvasGroup.alpha = 1f;
        StartCoroutine(Transition(1f, 0f, _events.OnFadeInEnds));
    }

    public IEnumerator Transition(float startValue, float endValue, UnityEvent onTransitionEnd)
    {
        _timer = 0f;

        while (_canvasGroup.alpha != endValue)
        {
            var deltaTime = _igoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
            _timer += _transitionTime * deltaTime;
            _canvasGroup.alpha = Mathf.Lerp(startValue, endValue, _timer);
            yield return null;
        }

        onTransitionEnd?.Invoke();
    }
}