﻿using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{
    [SerializeField] Image _image;
    public void Setup(Item item)
    {
        _image.sprite = item.Icon;
    }
}