﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ItemSlotHud : MonoBehaviour
{
    [SerializeField] Sprite _emptySlotSprite;
    
    private Image _itemSlot;

    private void Awake()
    {
        _itemSlot = GetComponent<Image>();
    }

    public void EquipItem(Sprite itemSprite)
    {
        _itemSlot.sprite = itemSprite;
    }

    public void ClearSlot()
    {
        _itemSlot.sprite = _emptySlotSprite;
    }
}
