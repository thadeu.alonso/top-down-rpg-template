﻿using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public Transform CameraTarget;
    public GameObject MapEdges;
    public List<GameObject> MapEntities;

    public void ActivateMap()
    {
        MapEdges.SetActive(true);

        foreach (var ent in MapEntities)
        {
            ent.SetActive(true);
        }
    }

    public void DeactivateMap()
    {
        MapEdges.SetActive(false);

        foreach (var ent in MapEntities)
        {
            ent.SetActive(false);
        }
    }
}
