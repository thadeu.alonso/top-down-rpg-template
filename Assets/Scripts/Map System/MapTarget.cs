﻿using System.Collections.Generic;

[System.Serializable]
public class MapTarget
{
    public Map Map;
    public List<string> ConnectedToLocations;
    public List<MapNeighbour> Neighbours;
}
