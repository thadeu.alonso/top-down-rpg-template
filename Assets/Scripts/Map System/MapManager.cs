﻿using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    [SerializeField] StringGlobalVariable _playerLastLocation;
    [SerializeField] List<MapTarget> _maps;
    [SerializeField] Vector3UnityEvent OnNextMap;
    [SerializeField] Vector3UnityEvent OnSetTarget;

    private MapTarget _currentMapTarget = null;

    public void ActivateMapBasedOnLastLocation()
    {
        foreach (var mapTarget in _maps)
        {
            if(mapTarget.ConnectedToLocations.Count > 0)
            {
                foreach (var connectedLocation in mapTarget.ConnectedToLocations)
                {
                    if(connectedLocation == _playerLastLocation.Value)
                    {
                        _currentMapTarget = mapTarget;
                        ActivateCurrentMap();
                    }
                }
            }
        }

        if(_currentMapTarget == null && _maps.Count > 0)
        {
            _currentMapTarget = _maps[0];
            ActivateCurrentMap();
        }
    }

    public void GoToDirection(int directionIndex)
    {
        EDirection direction = (EDirection)directionIndex;
        var nextMapTarget = _currentMapTarget.Neighbours.Find(m => m.Direction == direction);

        if(nextMapTarget != null)
        {
            _currentMapTarget.Map.DeactivateMap();
            var map = nextMapTarget.Map;
            OnNextMap?.Invoke(map.CameraTarget.position);
            _currentMapTarget = _maps.Find(target => target.Map == map);
        }
    }

    public void ActivateCurrentMap()
    {
        _currentMapTarget.Map.ActivateMap();
        OnSetTarget?.Invoke(_currentMapTarget.Map.CameraTarget.position);

        foreach (var mt in _maps)
        {
            if (mt == _currentMapTarget)
                continue;

            mt.Map.DeactivateMap();
        }
    }
}