﻿using UnityEngine;
using UnityEngine.Events;

public class EnterMapBehaviour : MonoBehaviour
{
    [SerializeField] EDirection _direction;
    [SerializeField] IntUnityEvent OnEnterMap;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnEnterMap?.Invoke((int)_direction);
    }
}
