﻿using UnityEngine;

public class DialogInteractable : DialogBase
{
    [SerializeField] DialogSequence _dialog;

    protected override void SetCurrentDialog()
    {
        _currentDialog = _dialog;
    }
}
