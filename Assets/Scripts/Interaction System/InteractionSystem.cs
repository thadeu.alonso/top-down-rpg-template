﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class InteractionSystem : MonoBehaviour, IDirectionable
{
    [Header("General")]
    [SerializeField] LayerMask _interactableLayer;
    [SerializeField] float _maxDirectionalDistance = 1f;
    [SerializeField] float _touchRadius;
    [Header("Debug")]
    [SerializeField] bool _showRay;
    [SerializeField] bool _showCircleRay;
    [Header("Events")]
    [SerializeField] UnityEvent OnInteract;

    public Action OnInteractionStarts;
    public Action OnInteractionEnds;
    public bool IsInteracting { get; private set; }

    private Vector2 _facingDirection;
    private IInteractable _currentInteractableOnSight;

    private void Update()
    {
        if (!IsInteracting)
        {
            DetectInteractableOnSight();
            DetectInteractableOnTouch();
        }
    }

    private void DetectInteractableOnTouch()
    {
        Collider2D hit = Physics2D.OverlapCircle(transform.position, _touchRadius, _interactableLayer);

        if (hit != null)
        {
            IInteractable interactable = hit.GetComponent<IInteractable>();

            if (interactable != null)
            {
                interactable.Interact(gameObject);
            }
            else
            {
                Debug.LogWarning($"Object {hit.transform.gameObject.name} is on Interactable layer but doesn't have a script Interactable");
            }
        }
    }

    private void DetectInteractableOnSight()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, _facingDirection, _maxDirectionalDistance, _interactableLayer);

        if (hit.collider != null)
        {
            IInteractable interactable = hit.transform.GetComponent<IInteractable>();

            if (interactable != null)
            {
                _currentInteractableOnSight = interactable;
            }
            else
            {
                Debug.LogWarning($"Object {hit.transform.gameObject.name} is on Interactable layer but doesn't have a script Interactable");
            }
        }
        else
        {
            _currentInteractableOnSight = null;
        }
    }

    public void TryToInteract()
    {
        if (_currentInteractableOnSight == null)
            return;

        if (!_currentInteractableOnSight.QuickInteraction)
            IsInteracting = !_currentInteractableOnSight.IsDone;

        InteractOnSight();
    }

    private void InteractOnSight()
    {
        _currentInteractableOnSight.Interact(gameObject);
        OnInteract?.Invoke();
    }

    public void CancelInteraction()
    {
        IsInteracting = false;
    }

    public void SetDirectionX(float x)
    {
        if (x != 0f)
            _facingDirection = new Vector2(x, 0f);
    }

    public void SetDirectionY(float y)
    {
        if (y != 0f)
            _facingDirection = new Vector2(0f, y);
    }

    public void SetDirection(Vector2 direction)
    {
        if (direction != Vector2.zero)
            _facingDirection = direction;
    }

    private void OnDrawGizmos()
    {
        if (_showRay && _maxDirectionalDistance > 0f)
        {
            Gizmos.color = _currentInteractableOnSight != null ? Color.green : Color.red;
            Gizmos.DrawRay(transform.position, _facingDirection * _maxDirectionalDistance);
        }

        if (_showCircleRay && _touchRadius > 0f)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, _touchRadius);
        }
    }

}