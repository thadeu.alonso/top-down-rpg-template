﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportManager : MonoBehaviour
{
    [SerializeField] GameObject playerObject;
    [SerializeField] GameObject destination;
    [SerializeField] Animator canvasAnim;
    [SerializeField] float loadTime;


   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerObject = collision.gameObject;
            canvasAnim.SetBool("FadeIn", false);
            StartCoroutine(LoadingAnim());
            

        }
    }

    IEnumerator LoadingAnim()
    {
        canvasAnim.SetBool("FadeOut", true);
        yield return new WaitForSeconds(loadTime);
        canvasAnim.SetBool("FadeOut", false);
        playerObject.transform.position = destination.transform.position;
        canvasAnim.SetBool("FadeIn", true);

    }

}
