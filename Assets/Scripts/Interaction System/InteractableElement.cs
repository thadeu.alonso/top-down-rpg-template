﻿using UnityEngine;

public class InteractableElement : MonoBehaviour, IInteractable
{
    [SerializeField] public GameObjectUnityEvent OnInteract;

    public bool IsDone { get; set; }
    public bool QuickInteraction { get; set; }

    private void Awake()
    {
        QuickInteraction = true;
    }

    public void Interact(GameObject actor)
    {
        OnInteract?.Invoke(actor);
    }
}