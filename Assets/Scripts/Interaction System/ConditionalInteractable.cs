﻿using UnityEngine;
using UnityEngine.Events;

public class ConditionalInteractable : MonoBehaviour, IInteractable
{
    [Header("Conditions")]
    [SerializeField] BoolGlobalVariable _conditionalValue;
    [Header("General")]
    [SerializeField] bool _quickInteraction;
    [SerializeField] UnityEvent OnConditionTrue;
    [SerializeField] UnityEvent OnConditionFalse;

    private bool _alreadyInteracted;

    public bool IsDone { get; set; }
    public bool QuickInteraction => _quickInteraction;

    public void Interact(GameObject actor)
    {
        if(!_alreadyInteracted)
        {
            if (_conditionalValue.Value)
            {
                OnConditionTrue?.Invoke();

                if (_quickInteraction)
                {
                    _alreadyInteracted = true;
                }
            }
            else
            {
                OnConditionFalse?.Invoke();
            }
        }
    }
}