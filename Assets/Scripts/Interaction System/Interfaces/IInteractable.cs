﻿using UnityEngine;

public interface IInteractable
{
    bool QuickInteraction { get; }
    bool IsDone { get; }
    void Interact(GameObject actor);
}
