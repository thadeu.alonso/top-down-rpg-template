﻿using UnityEngine;
using UnityEngine.Events;

public class ItemConditionalInteractable : MonoBehaviour, IInteractable
{
    [Header("Conditions")]
    [SerializeField] Inventory _inventory;
    [SerializeField] Item _itemToCheck;
    [SerializeField] bool _consumeItem;
    [Header("General")]
    [SerializeField] bool _quickInteraction;
    [SerializeField] UnityEvent OnConditionTrue;
    [SerializeField] UnityEvent OnConditionFalse;

    private bool _alreadyInteracted;

    public bool IsDone { get; set; }
    public bool QuickInteraction => _quickInteraction;

    public void Interact(GameObject actor)
    {
        if (!_alreadyInteracted)
        {
            if (_inventory.HasItem(_itemToCheck))
            {
                OnConditionTrue?.Invoke();

                if (_consumeItem)
                    _inventory.RemoveItem(_itemToCheck);

                if (_quickInteraction)
                {
                    _alreadyInteracted = true;
                }
            }
            else
            {
                OnConditionFalse?.Invoke();
            }
        }
    }
}
