﻿using UnityEngine;
using UnityEngine.Events;

public class ConditionalDialogInteractable : DialogBase
{
    [SerializeField] BoolGlobalVariable _conditionalValue;
    [SerializeField] DialogSequence _dialogOnTrue;
    [SerializeField] DialogSequence _dialogOnFalse;

    protected override void SetCurrentDialog()
    {
        _currentDialog = _conditionalValue.Value ? _dialogOnTrue : _dialogOnFalse;
    }
}