﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct DialogEvents
{
    public StringUnityEvent OnDialogStarts;
    public UnityEvent OnDialogEnds;
}

public abstract class DialogBase : MonoBehaviour, IInteractable
{
    [Header("Events")]
    [SerializeField] DialogEvents DialogEvents;

    public bool IsDone { get; set; }
    public bool QuickInteraction { get; set; }

    protected bool _dialogStarted;
    protected int _lineIndex;
    protected DialogSequence _currentDialog;

    protected abstract void SetCurrentDialog();

    public void Interact(GameObject actor)
    {
        SetCurrentDialog();

        if (IsDone)
        {
            DialogEvents.OnDialogEnds?.Invoke();
            IsDone = false;
            _dialogStarted = false;
            return;
        }

        if (!_dialogStarted)
        {
            _dialogStarted = true;
            _lineIndex = 0;
            IsDone = false;
        }

        SayNextLine();
    }

    private void SayNextLine()
    {
        string line = _currentDialog.Sequence[_lineIndex].line;
        DialogEvents.OnDialogStarts?.Invoke(line);
        _lineIndex++;

        if (_lineIndex == _currentDialog.Sequence.Count)
        {
            _dialogStarted = false;
            IsDone = true;
        }
    }
}
