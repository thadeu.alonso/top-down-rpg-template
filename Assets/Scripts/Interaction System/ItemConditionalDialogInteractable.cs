﻿using UnityEngine;

public class ItemConditionalDialogInteractable : DialogBase
{
    [SerializeField] Inventory _inventory;
    [SerializeField] Item _itemToCheck;
    [SerializeField] DialogSequence _hasItemSequence;
    [SerializeField] DialogSequence _dontHaveItemSequence;

    protected override void SetCurrentDialog()
    {
        _currentDialog = _inventory.HasItem(_itemToCheck) ? _hasItemSequence : _dontHaveItemSequence;
    }
}