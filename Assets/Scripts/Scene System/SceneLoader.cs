﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] UnityEvent OnStartToLoadScene;
    [SerializeField] UnityEvent OnFinishToLoadScene;
    [SerializeField] StringUnityEvent OnLoadedScene;

    private AsyncOperation _currentAsyncOperation;

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
        OnLoadedScene?.Invoke(scene.name);
    }

    public void LoadSceneAsync(string sceneName)
    {
        StartCoroutine(LoadAsync(sceneName));
        OnStartToLoadScene?.Invoke();
    }

    public void ActivateLoadedScene()
    {
        if (_currentAsyncOperation != null)
            _currentAsyncOperation.allowSceneActivation = true;
    }

    private IEnumerator LoadAsync(string sceneName)
    {
        _currentAsyncOperation = SceneManager.LoadSceneAsync(sceneName);
        _currentAsyncOperation.allowSceneActivation = false;

        while (!_currentAsyncOperation.isDone)
        {
            yield return null;
        }
        
        OnFinishToLoadScene?.Invoke();
    }
}