﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct MoveObjectEvents
{
    public UnityEvent OnStartMove;
    public UnityEvent OnDestinationReached;
}

public class MoveObject : MonoBehaviour
{
    [SerializeField] bool _constantSpeed;
    [SerializeField] float _transitionEndDistance;
    [SerializeField] float _transitionSpeed;
    [SerializeField] MoveObjectEvents _events;

    public MoveObjectEvents Events => _events;
    public bool IsMoving { get; set; }
    
    private float _timer;

    public void SetPosition(Vector3 position)
    {
        var newPosition = new Vector3(position.x, position.y, transform.position.z);
        transform.position = newPosition;
    }

    public void Move(Vector3 initialPosition)
    {
        StartMove(initialPosition);
    }

    public void Move(GameObject target)
    {
        StartMove(transform.position);
    }

    private void StartMove(Vector3 destination)
    {
        StartCoroutine(InterpolatePosition(destination));
    }

    private IEnumerator InterpolatePosition(Vector3 direction)
    {
        _timer = 0f;
        Vector3 destination = direction;
        bool reachDestination = Vector2.Distance(transform.position, destination) <= _transitionEndDistance;
        IsMoving = true;
        _events.OnStartMove?.Invoke();

        while (!reachDestination)
        {
            _timer += _transitionSpeed * Time.unscaledDeltaTime;
            Vector3 newPosition = _constantSpeed 
                ? Vector3.MoveTowards(transform.position, destination, _transitionSpeed * Time.unscaledDeltaTime) 
                : Vector3.Lerp(transform.position, destination, _timer);
            newPosition.z = transform.position.z;
            transform.position = newPosition;
            reachDestination = Vector2.Distance(transform.position, destination) <= _transitionEndDistance;
            yield return null;
        }

        Vector3 finalPosition = new Vector3(destination.x, destination.y, transform.position.z);
        transform.position = finalPosition;
        IsMoving = false;
        _events.OnDestinationReached?.Invoke();
    }
}