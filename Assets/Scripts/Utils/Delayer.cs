﻿using System;
using System.Collections;
using UnityEngine;

public static class Delayer
{
    public static void DoAfterTime(MonoBehaviour source, Action action, float delay)
    {
        source.StartCoroutine(Delay(action, delay));
    }

    private static IEnumerator Delay(Action action, float delay)
    {
        yield return new WaitForSecondsRealtime(delay);
        action?.Invoke();
    }
}
